<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Ob dostopu do spletne aplikacije vrne glavno index.php stran.
 */
$app->get('/', function () use ($app) {
    return view('index');
});

/**
 * Izvrši funkcijo weather($stationID) iz InfoController nadzornika,
 * ki vrne vremenske podatke za podano vremensko postajo.
 */
$app->get('/api/v1/weather/{stationID}', 'InfoController@weather');

/**
 * Izvrši funkcijo feed($feedID) iz InfoController nadzornika,
 * ki vrne novice iz podanega RSS feeda.
 */
$app->get('/api/v1/feed/{feedID}', 'InfoController@feed');