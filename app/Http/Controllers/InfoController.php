<?php

namespace App\Http\Controllers;

class InfoController extends Controller
{
    /**
     * Zbirka ključev RSS feedov in njihovih URLjev.
     * @var array
     */
    private $feeds = array(
            'zurnal24' => 'http://www.zurnal24.si/index.php?ctl=show_rss',
            '24ur' => 'http://www.24ur.com/rss',
            'rtvslo' => 'http://rtvslo.si/novice/rss/',
            'dnevnik' => 'https://www.dnevnik.si/rss',
            'rac_novice' => 'http://www.racunalniske-novice.com/rss-novice/',
            'slo_novice' => 'http://www.slovenskenovice.si/rss',
            'vecer' => 'http://www.vecer.com/rubrika/feed',
            'siol' => 'http://siol.net/feeds/latest'
        );

    /**
     * Funkcija, ki pridobi najnovejše vremenske podatke za podano 
     * vremensko postajo in odjemalcu vrne temperaturo ter vlažnost.
     * @param  $stationID - Ključ vremenske postaje.
     * @return $items - Tabela s potrebnimi vremenskimi podatki. 
     */
    public function weather($stationID)
    {
        $url = 'http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_' . $stationID . '_latest.xml';
        $xml = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA) or die("feed not loading");
        $items = array();
        $items['temperature'] = (string) $xml->metData->t;
        $items['humidity'] = (string) $xml->metData->rh;
        return $items;
    }

    /**
     * Funkcija, ki pridobi izbrani RSS feed in odjemalcu vrne novice..
     * @param  $feedID - Ključ iz tabele, ki vsebuje RSS feede.
     * @return $items - JSON enkodirana tabela z novicami.
     */
    public function feed($feedID)
    {
        $url = $this->feeds[$feedID];
        $xml = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA) or die("feed not loading");
        $items = array();
        foreach($xml->channel->item as $item){
            array_push($items, $item);
        }
        return json_encode($items);
    }
}