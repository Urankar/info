<!DOCTYPE html>
<html lang="en" ng-app="info">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Info</title>
	<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Alegreya:400,400italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,900,200,200italic,300italic,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- Angular View - Tu se prikazuje izbrani angular pogled. 
		 Pogledi so nastavljeni v datoteki /js/paths.js -->
	<div ui-view></div>

	<!-- Components -->
	<script src="/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="/bower_components/angular/angular.min.js"></script>
	<script src="/vendor/angular-sanitize.min.js"></script>
	<script src="/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
	<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Module and Paths -->
	<script src="/js/app.js"></script>
	<script src="/js/paths.js"></script>

	<!-- Services -->
	<script src="/js/services/infoDataService.js"></script>

	<!-- Directives -->
	<script src="/js/directives/feedDirective.js"></script>

	<!-- Controllers -->
	<script src="/js/controllers/WeatherController.js"></script>
	<script src="/js/controllers/FeedController.js"></script>

</body>
</html>