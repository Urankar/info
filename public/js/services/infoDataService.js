angular.module('info')
.factory('infoData', function(){
	var data = {
		stations: {
			'1386': 'BABNO-POL',
			'5292': 'NOVA-GOR_BILJE',
			'4267': {'Bohinjska Češnjica': 'BOHIN-CES', 'Rudno polje': 'RUDNO-POL', 'Default': 'BOHIN-CES'},
			'4224': 'BORST_GOREN-VAS',
			'5230': 'BOVEC',
			'5223': 'BREGINJ',
			'5242': 'BUKOV-VRH',
			'3000': 'CELJE',
			'1380': 'CERKN-JEZ',
			'4228': 'DAVCA',
			'8340': 'CRNOMELJ',
			'5270': {'Dolenje': 'AJDOV-INA_DOLENJE', 'Otlica': 'OTLICA', 'Default': 'AJDOV-INA_DOLENJE'},
			'2211': 'GACNIK',
			'6221': 'GODNJE',
			'3342': 'GORNJ-GRA',
			'2208': 'HOCKO-POH',
			'1430': 'HRASTNIK',
			'5280': 'IDRIJA_CISTI-NAP',
			'6250': 'ILIRS-BIS',
			'1338': 'ISKRBA',
			'3305': 'JERONIM',
			'2259': 'JERUZ-LEM',
			'4206': 'JEZERSKO',
			'1242': 'KAMNI-BIS',
			'1330': 'KOCEVJE',
			'6000': 'KOPER_KAPET-IJA',
			'9264': 'GORICKO_KRAJI-PAR',
			'4000': 'KRANJ',
			'4281': {'Kredarica': 'KREDA-ICA', 'Zgornja Radovna': 'ZGORN-RAD', 'Default': 'KREDA-ICA'},
			'5222': 'KRN',
			'8270': 'KRSKO_NEK',
			'4207': 'KRVAVEC',
			'9220': 'LENDAVA',
			'2312': 'MARIBOR_SLIVNICA',
			'4210': 'LJUBL-ANA_BRNIK',
			'8263': 'CERKLJE_LETAL-SCE',
			'4258': 'LESCE',
			'6333': 'PORTOROZ_SECOVLJE',
			'8290': 'LISCA',
			'1270': 'LITIJA_GRBIN',
			'1000': 'LJUBL-ANA_BEZIGRAD',
			'3335': 'LOGAR-DOL',
			'1370': 'LOGATEC',
			'6501': 'KOPER_LUKA',
			'8295': 'MALKOVEC',
			'2000': 'MARIBOR_TABOR',
			'9000': 'MURSK-SOB',
			'5000': 'NOVA-GOR',
			'1385': {'Nova Vas': 'NOVA-VAS_BLOKE', 'Topol': 'TOPOL', 'Default': 'NOVA-VAS_BLOKE'},
			'8000': {'Novo Mesto': 'NOVO-MES', 'Sevno': 'SEVNO', 'Default': 'NOVO-MES'},
			'1337': 'OSILNICA',
			'6215': 'SKOCJAN',
			'1355': 'PASJA-RAV',
			'6330': 'PIRAN_OCEAN-BOJ',
			'4270': 'PLANI-POD',
			'3254': 'PODCE-TEK_ATOMS-TOP',
			'6230': 'POSTOJNA',
			'2250': 'PTUJ_TERME',
			'3330': 'RADEG-NDA',
			'9252': 'RADENCI',
			'4283': 'RATECE',
			'2390': 'RAVNE_NA-KOR',
			'3250': 'ROGAS-SLA',
			'3214': 'ROGLA',
			'2380': 'SLOVE-GRA',
			'5282': 'SEBRE-VRH',
			'5220': 'TOLMIN_VOLCE',
			'8210': 'TREBNJE',
			'1222': 'TROJANE_LIMOVCE',
			'5211': 'VEDRIJAN',
			'3320': 'VELENJE_TES',
			'1315': 'VELIK-LAS',
			'1360': 'VRHNIKA',
			'5274': 'ZADLOG',
			'2362': 'ZGORN-KAP',
			'4229': 'ZGORN-SOR'
		}
		,
		feeds: [
			{ 
				name: 'Žurnal 24',
				feedID: 'zurnal24' 
			},
			{ 
				name: '24ur.com',
				feedID: '24ur' 
			},
			{ 
				name: 'RTV Slovenija',
				feedID: 'rtvslo' 
			},
			{ 
				name: 'Dnevnik',
				feedID: 'dnevnik' 
			},
			{ 
				name: 'Računalniške novice',
				feedID: 'rac_novice' 
			},
			{ 
				name: 'Slovenske novice',
				feedID: 'slo_novice' 
			},
			{ 
				name: 'Večer',
				feedID: 'vecer' 
			},
			{ 
				name: 'SiOL',
				feedID: 'siol' 
			}
		]
	}

	return data;
});