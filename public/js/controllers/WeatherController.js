/**
 * WeatherController je nadzornik, zadolžen za izvajalno logiko prikaza vremena.
 * Uporablja storitev infoData, iz katere pridobi vremenske postaje.
 */
angular.module('info')
.controller('WeatherController', ['$scope', '$http', '$interval', 'infoData', function($scope, $http, $interval, infoData){

	/**
	 * getWeather() pridobi koordinate trenutne lokacije
	 * uporabnika in ob uspehu požene funkcijo showPosition. 
	 */
	$scope.getWeather = function() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition, showError);
		} else {
			console.log('Geolocation is not supported by this browser.');
		}
	};

	/**
	 * showPosition() z uporabo google APIja pridobi informacije
	 * o trenutni lokaciji in iz njih pridobi ime mesta in poštno
	 * številko ter požene funkcijo getWeatherData. Če ne najde mesta
	 * ali poštne številke, vrne napako.
	 * @param position - Vsebuje koordinate.
	 */
	var showPosition = function(position) {
		var latlon = position.coords.latitude + ',' + position.coords.longitude;
		var count = 0;
		var postal_code = '';
		var stationIDtemp = '';
		$http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latlon + '&key=AIzaSyAQOMMsnL8B_HR1AHr6w8oyPm2-8tzTvBI')
		.then(function successCallback(gApiRes) {
			for(let result of gApiRes.data.results){
				for(let value of result.address_components){
					if(value.types.includes('locality')){
						$scope.locality = value.long_name;
						count++;
					} else if(value.types.includes('postal_code')){
						postal_code = value.long_name;
						count++;
					}
					if(count == 2) {
						getWeatherData(postal_code);
						break;
					}
				}
				if(count == 2) break;
			}
			if(count < 2) console.log('I can\'t find anything useful.');
		},
		function errorCallback(gApiRes){
			console.log(gApiRes);
		});
	}

	/**
	 * showError vrne napako, če geolocation ne uspe vrniti lokacije.
	 */
	var showError = function(error) {
		switch(error.code) {
			case error.PERMISSION_DENIED:
			console.log('User denied the request for Geolocation.');
			break;
			case error.POSITION_UNAVAILABLE:
			console.log('Location information is unavailable.');
			break;
			case error.TIMEOUT:
			console.log('The request to get user location timed out.');
			break;
			case error.UNKNOWN_ERROR:
			console.log('An unknown error occurred.');
			break;
		}
	}

	/**
	 * getWeatherData() uporabi poštno številko mesta trenutne lokacije
	 * uporabnika, uporabi funkcijo closest(), da najde najbližjo poštno
	 * številko mesta, kjer obstaja vremenska postaja, in pridobi njen
	 * ključ. V primeru, da je z eno poštno številko povezanih več
	 * vremenskih postaj, si pomaga z imenom mesta trenutne lokacije.
	 * Ko pridobi ključ vremenske postaje, iz serverja pridobi temperaturo
	 * in vlažnost za to vremensko postajo.
	 */
	var getWeatherData = function(postal_code) {
		var arr = Object.keys(infoData.stations);
		stationIDtemp = infoData.stations[closest(arr, postal_code)];
		if(typeof stationIDtemp == 'object'){
			$scope.stationID = stationIDtemp[$scope.locality.toLowerCase()] || stationIDtemp['Default'];
		} else {
			$scope.stationID = stationIDtemp;
		}
		return $http.get('/api/v1/weather/' + $scope.stationID)
		.then(function successCallback(weatherRes) {
			$scope.weather = weatherRes.data;
		},
		function errorCallback(weatherRes){
			console.log(weatherRes);
		});
	}

	/**
	 * closest() za podano poštno številko v tabeli vremenskih postaj
	 * poišče poštno številko, ki ji je numerično najbližje, kar pomeni,
	 * da je tudi mesto geografsko najbližje.
	 */
	var closest = function(arr, closestTo) {
		var closest = Math.max.apply(null, arr);
		for(var i = 0; i < arr.length; i++){ 
			if(arr[i] >= closestTo && arr[i] < closest) closest = arr[i]; 
		}
		return closest;
	}

	/**
	 * Ključ vremenske postaje.
	 * @type {String}
	 */
	$scope.stationID = '';
	/**
	 * Mesto, ki se nahaja na trenutni lokaciji.
	 * @type {String}
	 */
	$scope.locality = '';
	/**
	 * Vsebuje temperaturo in vlažnost.
	 * @type {Object}
	 */
	$scope.weather = {
		temperature: '',
		humidity: ''
	};

	/**
	 * Ob začetku se takoj požene funkcija getWeather, ki pridobi
	 * temperaturo in vlažnost za trenutno lokacijo uporabnika, 
	 * takoj za tem pa se zažene interval, ki funkcijo avtomatsko 
	 * zažene vsako minuto.
	 */
	$scope.getWeather();
	$interval(function() {
		$scope.getWeather();
	}, 60000);


}]);