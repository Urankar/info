/**
 * FeedController je nadzornik, zadolžen za izvajalno logiko RSS feedov.
 * Uporablja storitev infoData, iz katere pridobi RSS feede.
 */
angular.module('info')
.controller('FeedController', ['$scope', '$http', '$interval', 'infoData', function($scope, $http, $interval, infoData){

	/**
	 * feedFunction() iz serverja dobi novice za izbrani RSS feed.
	 * Podatke shrani v spremenljivko currentFeed, iz katere se
	 * napolnijo novice v spletni aplikaciji.
	 */
	$scope.feedFunction = function(){
		$http.get('/api/v1/feed/' + $scope.feedSelect)
		.then(function successCallback(feedRes){
			$scope.currentFeed = feedRes.data;
		},
		function errorCallback(feedRes){
			console.log(feedRes);
		});
	};
	
	/**
	 * feedSelect() je nastavljen na začetni RSS feed, katerega
	 * novice se prikažejo ob dostopu do spletne aplikacije, in
	 * se spreminja dinamično, ko uporabnik izbere drug RSS feed.
	 * @type {String}
	 */
	$scope.feedSelect = 'zurnal24';

	/**
	 * feeds pridobi vse možne RSS feede, ki so shranjeni v storitvi
	 * infoData, in iz njih ustvari primeren dropdown seznam.
	 * @type {[array]}
	 */
	$scope.feeds = infoData.feeds;

	/**
	 * Ob začetku se takoj požene funkcija feedFunction, ki napolni
	 * novice za začetni RSS feed, takoj za tem pa se zažene interval,
	 * ki funkcijo avtomatsko zažene vsako minuto.
	 */
	$scope.feedFunction();
	$interval(function() {
		$scope.feedFunction();
	}, 60000);

}]);