angular.module('info')
  .directive('news', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        data: '='
      },
      templateUrl: 'templates/news_item.html'
    };
  });