angular.module('info')

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('main', {
      url: '/',
      templateUrl: '/views/index.html'
    });

  $urlRouterProvider.otherwise('/');
}]);